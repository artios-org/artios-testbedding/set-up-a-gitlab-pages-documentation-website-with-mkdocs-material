# Set up a GitLab Pages documentation website with Material for MkDocs

This guide will show you how to create documentation with Material for MkDocs, and how to upload it to Github Pages, [like this example](https://artios-org.gitlab.io/artios-testbedding/set-up-a-gitlab-pages-documentation-website-with-mkdocs-material/).

## Prerequisites

Have created a project in GitLab and cloned it locally.

## Guide

_Highly inspired from the following guide: [Get started with Material for MkDocs](https://squidfunk.github.io/mkdocs-material/getting-started/)._

### Install Material for MkDocs on your machine

```sh
pip install mkdocs-material
```

### Create your site

```sh
mkdocs new .
```

This will create the following structure:
```sh
.
├─ docs/
│  └─ index.md
└─ mkdocs.yml
```

### Configure your site

you can configure your site by adding lines to your `mkdocs.yml`

```yml
site_name: Set up a GitLab Pages documentation website with MkDocs Material
site_author: Artios Organization
theme:
  icon: 
    repo: fontawesome/brands/gitlab
  name: material
repo_url: https://gitlab.com/artios-org/artios-testbedding/set-up-a-gitlab-pages-documentation-website-with-mkdocs-material
```

For more details on the configuration, please see the [Material for MkDocs documentation](https://squidfunk.github.io/mkdocs-material/creating-your-site/#advanced-configuration)

### Previewing as you write

MkDocs includes a live preview server, so you can preview your changes as you write your documentation. The server will automatically rebuild the site upon saving.
```sh
mkdocs serve 
```
Point your browser to [localhost:8000](localhost:8000) and you should see your site.

### Publish your site to GitLab Pages

At the root of your repository, create a task definition named .gitlab-ci.yml and copy and paste the following contents:

```yml
image: python:latest
pages:
  stage: deploy
  only:
    - masters
    - main
  script:
    - pip install mkdocs-material
    - mkdocs build --site-dir public
  artifacts:
    paths:
      - public
```

### Add the .gitignore file to your repository

Our `.gitignore` looks like that :

```sh
**/public
**/site
```

Those are the folder created by mkdocs to save the static file of the site.

# BONUS : Redirect your GitLab Pages to another URL

We redirected "docs.artios.ch" to "artios-org.gitlab.io/artiofm". Here is a small guide that explains how we did it.

## Prerequisites

- Have done the above guide.
- Have an Infomaniak account (For our situation only, all domain name providers work)

## Guide
_Highly inspired from the following guide: [Get started with Custom domains for GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/index.html)._


### Purchase your domain name

First, we checked the availability of the domain name on the site [Infomaniak](https://shop.infomaniak.com/order/select/domain). Then we chose the options that accompany the domain name. We only chose the "Domain Privacy" option for identity protection reasons. We did not choose the option "DNS Fast Anycast", because our service is intended to be local at the beginning, with a small frequency of use. Finally, we haven't chosen any of the options for hosting our site offered by Infomaniak. At the beginning, we only need Github Pages and we will choose another cloud provider, because of the prices and customization parameters offered by Infomaniak.

### DNS redirection

In the GitLab Pages options you should be able to add a new domain. Give it the domain name you want to link to your GitLab Pages. In our example it will be `docs.artios.ch`.

After adding your new domain, you should arrive at a page that gives you two pieces of information: DNS and Verification status:

```yml
DNS: docs.artios.ch ALIAS artios-org.gitlab.io.
Verification status: _gitlab-pages-verification-code.docs.artios.ch TXT gitlab-pages-verification-code=passcode
```

Your domain name provider should have a page for defining the DNS zone. This is the link between your domain name and the servers that manage your services. 

On this page you should be able to add an entry. We will start with DNS :

```yml
Type: CNAME
Source: docs.artios.ch
Target: artios-org.gitlab.io.
TTL: 1 hour
```

And the second entry with the verification code :

```yml
Type: TXT
Source: _gitlab-pages-verification-code.docs.artios.ch
Target: gitlab-pages-verification-code=passcode
TTL: 1 hour
```

After that, go back to the general GitLab Pages settings on your project, and update your "Verification status"

Wait a little while for everything to fall into place and your site should be redirected